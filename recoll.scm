;;; Copyright © 2016 Al McElrath <hello@yrns.org>

(define-module (recoll)
  #:use-module ((guix licenses) #:select (gpl3))
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages aspell)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages python)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages search)
  #:use-module (gnu packages xml))

(define-public recoll
  (package
    (name "recoll")
    (version "1.22.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://www.lesbonscomptes.com/recoll/recoll-"
                           version ".tar.gz"))
       (sha256
        (base32 "072rlgysl4f908whcms00cvbqf0n2q0051rkjfa3786fsayad5bl"))))
    (build-system gnu-build-system)
    (arguments
     `(#:configure-flags '("--disable-webkit" ; no qtwebkit yet
                           "--with-inotify"
                           "--enable-recollq")))
    (native-inputs
     `(("bison" ,bison)
       ("libxslt" ,libxslt)
       ("python2" ,python-2)))
    (inputs
     `(("xapian" ,xapian)
       ("qt" ,qt-4)
       ("zlib" ,zlib)
       ("aspell" ,aspell)))
    (home-page "http://www.lesbonscomptes.com/recoll/")
    (synopsis "Full text search tool based on Xapian backend")
    (description
     "Recoll offers a powerful text extraction layer and a complete,
easy to use Qt graphical interface.  Recoll uses external applications
to index some file types.  You need to install them for the file types
that you wish to have indexed.  See
<http://www.lesbonscomptes.com/recoll/usermanual/usermanual.html#RCL.INSTALL.EXTERNAL>.")
    (license gpl3)))
