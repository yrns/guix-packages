(define-module (streamlink)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages video))

(define-public streamlink
  (package
    (inherit livestreamer)
    (name "streamlink")
    (version "0.0.2")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/streamlink/streamlink/releases/download/"
                    version "/streamlink-" version ".tar.gz"))
              (file-name (string-append "streamlink-" version ".tar.gz"))
              (sha256
               (base32
                "1967cgdpvllslv7ln1yzq2gvzwv6shvy6fb49g8fqh55zing8hy4"))))
    (description "Streamlink is a command-line utility that extracts streams
from various services and pipes them into a video playing application.")
    (home-page "https://streamlink.github.io")))
