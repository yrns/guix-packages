(define-module (ansible)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages python))

;; sovereign need pre-2.0 ansible!
(define-public ansible-1.9
  (package (inherit ansible)
    (name "ansible")
    (version "1.9.6")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ansible" version))
       (sha256
        (base32
         "0pgfh5z4w44sjgd77q6k769a5ipigjlm28zbpf2jhvz7n60kfxsh"))))))
