(define-module (st)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages suckless))

(define-public st-yrns
  (package
    (inherit st)
    (name "st-yrns")
    (source
     (origin (inherit (package-source st))
             (modules '((guix build utils)))
             ;; (snippet '(substitute* "config.def.h" (("Liberation Mono") "monospace")))
              (patches
               (list (search-patch "st-solarized-light.patch")))))))
